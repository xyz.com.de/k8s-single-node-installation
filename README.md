# Archlinux k8s with kubeadm 

This is how to install k8s v1.17 by kubeadm on Archlinux. We use the
standard [archlinux vagrant box](https://app.vagrantup.com/archlinux/boxes/archlinux).

This guide can be everywhere you want to use a single node k8s installation. To 
run k8s single node on a root server you should keep an eye on listen ports, use
an VPN to access the API server an just expose Ports you realy want to be exposed.

To route traffic through your single node installation by an nginx ingress proxy
take a look at my [nginx-ingress-controller example](https://gitlab.com/xyz.com.de/k8s-nginx-ingress-controller). 

## Installation

Additinal packages required in the vagrant box and reboot. Currently (2020-02-01)
the archlinux vagrant box does not include the netctl package. This cause problems
with the private network interface in our vagrant file. For this reason it makes 
sense to install the following packages, tritzen and [pack a new box](https://gist.github.com/srijanshetty/86aa6540d27736c1c11b) 
to continue.

```bash
sudo pacman --noconfirm -Suy \
    base-devel \
    curl \
    docker \
    ebtables \
    ethtool \
    git \
    netctl \
    socat \
    wget \
    unzip 
``` 

Install AUR package manger trizen

```bash
git clone https://aur.archlinux.org/trizen.git \
&& cd trizen \
&& makepkg -si --noconfirm
```

Set recommended [Docker settings](https://kubernetes.io/docs/setup/production-environment/container-runtimes/).

```bash
sudo mkdir /etc/docker/
```

```bash
sudo cat > /etc/docker/daemon.json <<EOF
{
  "exec-opts": ["native.cgroupdriver=systemd"],
  "log-driver": "json-file",
  "log-opts": {
    "max-size": "100m"
  },
  "storage-driver": "overlay2"
}
EOF
```

```bash
sudo systemctl start docker
```

We use [Cloudflares PKI and TLS toolkit](https://github.com/cloudflare/cfssl)

```bash
trizen --noconfirm -S cfssl
```

Make shure the required kernelmodules are enabled

```bash
sudo modprobe br_netfilter

sudo sysctl net.bridge.bridge-nf-call-iptables=1
```

Clear dockers firewall ruels

```bash  
sudo iptables -F
sudo iptables -t nat -F
```

**From now on it's easyer to run everythin as root.**

Install Container Network Interface

```bash
export CNI_VERSION="v0.7.1"
mkdir -p /opt/cni/bin
curl -L "https://github.com/containernetworking/plugins/releases/download/${CNI_VERSION}/cni-plugins-amd64-${CNI_VERSION}.tgz" | tar -C /opt/cni/bin -xz
mkdir -p /etc/cni/net.d
```

Install Container runtime Interface

```bash  
export CRICTL_VERSION="v1.17.0"
mkdir -p /opt/cni/bin
curl -L "https://github.com/kubernetes-sigs/cri-tools/releases/download/${CRICTL_VERSION}/crictl-${CRICTL_VERSION}-linux-amd64.tar.gz"  | tar -C /opt/cni/bin -xz
```

Set environment variable with the latest stable k8s release

```bash
RELEASE="$(curl -sSL https://dl.k8s.io/release/stable.txt)"
```

Download kubeadm, kubelet and kubectl (kubectl also could be installed from pacman)

```bash
cd /usr/local/bin
curl -L --remote-name-all https://storage.googleapis.com/kubernetes-release/release/${RELEASE}/bin/linux/amd64/{kubeadm,kubelet,kubectl}
chmod +x {kubeadm,kubelet,kubectl}
```

Install kubelet systemd service

```bash
curl -sSL "https://raw.githubusercontent.com/kubernetes/kubernetes/${RELEASE}/build/debs/kubelet.service" | sed "s:/usr/bin:/usr/local/bin:g" > /etc/systemd/system/kubelet.service
mkdir -p /etc/systemd/system/kubelet.service.d
systemctl daemon-reload
```

Get kubeadm.conf

```bash
curl -sSL "https://raw.githubusercontent.com/kubernetes/kubernetes/${RELEASE}/build/debs/10-kubeadm.conf" | sed "s:/usr/bin:/usrlocal/bin:g" > /etc/systemd/system/kubelet.service.d/10-kubeadm.conf
```

Enable and start kubelet service

```bash
systemctl enable kubelet && systemctl start kubelet
```

Now the system is prepared to run Kubernetes. Let's init the cluster.

```bash
kubeadm init --pod-network-cidr=10.244.0.0/16
```
```bash
mkdir -p $HOME/.kube
cp -i /etc/kubernetes/admin.conf $HOME/.kube/config
chown $(id -u):$(id -g) $HOME/.kube/config
```

Add [flannel](https://github.com/coreos/flannel) service

```bash  
kubectl apply -f https://raw.githubusercontent.com/coreos/flannel/master/Documentation/kube-flannel.yml
```

Cause we run a single node installation of k8s we need to make the master schedulable to run pods

```bash
kubectl taint nodes --all node-role.kubernetes.io/master-
```

## Debugging

During the whole installation process keep an eye on systems journal

```bash
journalctl -f
```